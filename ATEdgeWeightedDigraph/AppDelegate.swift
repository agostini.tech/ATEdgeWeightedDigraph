//
//  AppDelegate.swift
//  ATEdgeWeightedDigraph
//
//  Created by Dejan on 28/07/2018.
//  Copyright © 2018 agostini.tech. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        //testDirectedGraph()
        testBellmanFord()
        return true
    }
    
    private func testDirectedGraph()
    {
        let dublin = Vertex<String>("Dublin")
        let london = Vertex<String>("London")
        let paris = Vertex<String>("Paris")
        let amsterdam = Vertex<String>("Amsterdam")
        let montreal = Vertex<String>("Montreal")
        let sanFrancisco = Vertex<String>("San Francisco")
        
        let graph = EdgeWeightedDigraph<String>()
        // Don't forget to add the vertices
        graph.addVertex(dublin)
        graph.addVertex(london)
        graph.addVertex(paris)
        graph.addVertex(amsterdam)
        graph.addVertex(montreal)
        graph.addVertex(sanFrancisco)
        
        graph.addEdge(source: dublin, destination: london, weight: 20)
        graph.addEdge(source: dublin, destination: amsterdam, weight: 25)
        graph.addEdge(source: dublin, destination: paris, weight: 35)
        graph.addEdge(source: london, destination: paris, weight: 10)
        graph.addEdge(source: london, destination: montreal, weight: 200)
        graph.addEdge(source: london, destination: sanFrancisco, weight: 500)
        graph.addEdge(source: paris, destination: amsterdam, weight: 10)
        graph.addEdge(source: paris, destination: sanFrancisco, weight: 400)
        graph.addEdge(source: amsterdam, destination: montreal, weight: 300)
        graph.addEdge(source: amsterdam, destination: sanFrancisco, weight: 450)
        graph.addEdge(source: montreal, destination: sanFrancisco, weight: 200)
        graph.addEdge(source: sanFrancisco, destination: dublin, weight: 700)
        
        print("=====Vertices=====")
        print(graph.vertices)
        
        print("=====Edges=====")
        print(graph.edges())
        
        print("Destinations from London: \(graph.adjacentEdges(forVertex: london))")
        
        let dijkstra = DijkstraShortestPath(graph, source: london)
        
        print("Path to Dublin: ", dijkstra.pathTo(dublin))
        
        
        //////////// DAG ///////////
        let module1 = Vertex("Module 1")
        let module2 = Vertex("Module 2")
        let module3 = Vertex("Module 3")
        let module4 = Vertex("Module 4")
        let project = Vertex("Project")
        
        let projectGraph = EdgeWeightedDigraph<String>()
        projectGraph.addVertex(module1)
        projectGraph.addVertex(module2)
        projectGraph.addVertex(module3)
        projectGraph.addVertex(module4)
        projectGraph.addVertex(project)
        
        projectGraph.addEdge(source: module1, destination: module2, weight: 10)
        projectGraph.addEdge(source: module2, destination: module3, weight: 40)
        projectGraph.addEdge(source: module3, destination: module4, weight: 15)
        projectGraph.addEdge(source: module4, destination: module2, weight: 25)
        projectGraph.addEdge(source: module4, destination: project, weight: 5)
        
        let ewCycle = EdgeWeightedDirectedCycle(graph)
        
        print("EW Cycle: ", ewCycle.cycle)
        
        print("EW Cycle 2: ", ewCycle.cycle)
    }
    
    private func testBellmanFord() {
        
        // Vertices
        let newYork = Vertex("New York")
        let losAngeles = Vertex("Los Angeles")
        let dallas = Vertex("Dallas")
        let miami = Vertex("Miami")
        let london = Vertex("London")
        let amsterdam = Vertex("Amsterdam")
        let paris = Vertex("Paris")
        
        let graph = EdgeWeightedDigraph<String>()

        // Edges
        graph.addEdge(source: newYork, destination: amsterdam, weight: 55)
        graph.addEdge(source: newYork, destination: paris, weight: 80)
        graph.addEdge(source: newYork, destination: london, weight: 30)
        graph.addEdge(source: newYork, destination: losAngeles, weight: 100)
        graph.addEdge(source: losAngeles, destination: dallas, weight: 50)
        graph.addEdge(source: dallas, destination: losAngeles, weight: -65)
        graph.addEdge(source: dallas, destination: newYork, weight: 40)
        graph.addEdge(source: dallas, destination: miami, weight: 25)
        graph.addEdge(source: miami, destination: london, weight: 105)
        graph.addEdge(source: london, destination: miami, weight: 56)
        graph.addEdge(source: london, destination: amsterdam, weight: 15)
        graph.addEdge(source: amsterdam, destination: paris, weight: 5)
        graph.addEdge(source: paris, destination: amsterdam, weight: 15)
        
        let bellmanFord = BellmanFordShortestPath(graph, source: newYork)
        
        print("Has negative cycle: \(bellmanFord.hasNegativeCycle)")
        print("Negative cycle: \(bellmanFord.negativeCycle)")
    }
}
